run:
	g++ -o out.a main.cpp
	./out.a
	rm out.a

build-optimised:
	mkdir build
	g++ -o build/output$${CI_COMMIT_TAG}.a -O3 main.cpp
