# tic_tac_toe_cpp

tic tac toe with C++

## Requirements -
* g++ compiler
* make

## Quickstart

To build and run project
```shell
make run
```

To build optimised binary

```shell
make build-optimised
```
