#include <iostream>
#include <stdio.h>
#include <string>


using namespace std;

int turn;
char current_mark = '0';
char winner;
int row = 0;
int col = 0;
char area[3][3] =  {
	{' ', ' ', ' '},
	{' ', ' ', ' '},
	{' ', ' ', ' '},
};

int check_for_win() {
	if (
		(area[0][0] == current_mark && area[0][1] == current_mark && area[0][2] == current_mark)
		|| (area[1][0] == current_mark && area[1][1] == current_mark && area[1][2] == current_mark)
		|| (area[2][0] == current_mark && area[2][1] == current_mark && area[2][2] == current_mark)
		|| (area[0][0] == current_mark && area[1][0] == current_mark && area[2][0] == current_mark)
		|| (area[0][1] == current_mark && area[1][1] == current_mark && area[2][1] == current_mark)
		|| (area[0][2] == current_mark && area[1][2] == current_mark && area[2][2] == current_mark)
		|| (area[0][0] == current_mark && area[1][1] == current_mark && area[2][2] == current_mark)
		|| (area[0][2] == current_mark && area[1][1] == current_mark && area[2][0] == current_mark)
	) {
		return 1;
	} else {
		return 0;
	}
}


void draw_area() {
	for (int row = 0; row < 3; row++) {
		for (int col = 0; col < 3; col++) {
			cout << '|' << area[row][col] << '|';
		}
		cout << endl;
	}
}


int main()
{
	turn = 0;
	while (1) {
		draw_area();
		if (turn % 2 == 0) {
			current_mark = '0';
		} else {
			current_mark = 'X';
		}

		while (1) {
			string coordinates;
			cout << current_mark << " Is step now" << endl;
			cout << "Enter coordinates (row[int]-col[int])" << endl;
			cin >> coordinates;
			row = atoi(&coordinates[0]) - 1;
			col = atoi(&coordinates[2]) - 1;
			if (area[row][col] == ' ') {
				area[row][col] = current_mark;
				break;
			} else {
				cout << "INVALID POSITION" << endl;
			}
		}

		if (check_for_win()) {
			winner = current_mark;
			break;
		}
		if (turn == 8) {
			break;
		};
		turn++;
	}

	if (winner) {
		cout << "Winner " << current_mark << endl;
	} else {
		cout << "Friendship is win!!" << endl;
	}

	return 0;
}
